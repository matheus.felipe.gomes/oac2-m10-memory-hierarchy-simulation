IMG_NAME = oac2_m10_ubuntu
MAIN_DIR = oac2-m10-memory-hierarchy-simulation

build:
	docker build -t ${IMG_NAME} .

run:
	docker run -it --rm \
	-v ${PWD}:/root/${MAIN_DIR} \
	--name ${IMG_NAME}_cn \
	${IMG_NAME}

buildrun: build run